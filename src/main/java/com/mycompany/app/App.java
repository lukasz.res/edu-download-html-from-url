package com.mycompany.app;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Download html from url
 */
public class App {

    public static void main(String[] args) throws IOException {
        String url = "http://www.google.com";
        String html = Jsoup.connect(url)
                           .get()
                           .html();
        Document document = Jsoup.parse(html);
        String title = document.title();
        Elements links = document.select("a[href]");

        System.out.println(title);
        for (Element link : links) {
            System.out.println("link: " + link.attr("href"));
            System.out.println("text: " + link.text());
        }

        System.in.read();
    }
}
